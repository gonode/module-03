require('dotenv').config();

const app = require('express')();
const mongoose = require('mongoose');
const requireDir = require('require-dir');
const bodyParser = require('body-parser');
const raven = require('./app/services/sentry');

const PORT = 3000;
const dbConfig = require('./config/database');

mongoose.connect(dbConfig.url);
requireDir(dbConfig.modelsPath);

app.use(bodyParser.json());

app.use(raven.requestHandler());

app.use('/api', require('./app/routes'));

app.use(raven.errorHandler());

app.listen(PORT);
