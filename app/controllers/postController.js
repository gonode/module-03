const mongoose = require('mongoose');

const Post = mongoose.model('Post');

module.exports = {
    async create(req, res, next) {
        try {
            const post = await Post.create({ ...req.body, user: req.userId });

            return res.json(post);
        } catch (err) {
            return next(err);
        }
    },

    async toggle(req, res, next) {
        try {
            const postId = req.params.id;

            const post = await Post.findById(postId);

            if (!post) {
                return res.status(400).json({ error: 'Post doesn\'t exist!' });
            }

            const liked = post.likes.indexOf(req.userId);

            if (liked === -1) {
                post.likes.push(req.userId);
            } else {
                post.likes.splice(liked, 1);
            }

            await post.save();

            return res.json(post);
        } catch (err) {
            return next(err);
        }
    },

    async update(req, res, next) {
        try {
            const postId = req.params.id;

            const post = await Post.findByIdAndUpdate(postId, { ...req.body }, { new: true });

            return res.json(post);
        } catch (err) {
            return next(err);
        }
    },

    async destroy(req, res, next) {
        try {
            await Post.findByIdAndRemove(req.params.id);

            return res.send();
        } catch (err) {
            return next(err);
        }
    },
};
