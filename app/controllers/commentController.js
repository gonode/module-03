const mongoose = require('mongoose');

const User = mongoose.model('User');
const Post = mongoose.model('Post');
const Comment = mongoose.model('Comment');

module.exports = {
    async create(req, res, next) {
        try {
            const post = await Post.findById(req.params.id);
            const user = await User.findById(req.userId);

            if (post.user.toString() !== user.id) {
                if (user.friends.indexOf(post.user) === -1) {
                    return res.status(400).json({
                        error: 'You can\'t comment on a post from someone who is not your friend.',
                    });
                }
            }

            const comment = await Comment
                .create({
                    ...req.body,
                    user: req.userId,
                    post: req.params.id,
                });

            post.comments.push(comment.id);
            await post.save();

            return res.json(comment);
        } catch (err) {
            return next(err);
        }
    },

    async destroy(req, res, next) {
        try {
            const comment = await Comment.findById(req.params.id);

            if (!comment) {
                return res.status(400).json({ error: 'Comment does not exist!' });
            }

            const post = await Post.findById(comment.post);
            const commentIndex = post.comments.indexOf(comment.id);

            if (commentIndex !== -1) {
                post.comments.splice(commentIndex, 1);
                await post.save();
            }

            await Comment.findByIdAndRemove(req.params.id);

            return res.send();
        } catch (err) {
            return next(err);
        }
    },
};
