const mongoose = require('mongoose');

const User = mongoose.model('User');
const Post = mongoose.model('Post');

module.exports = {
    async me(req, res, next) {
        try {
            const user = await User.findById(req.userId);
            const postCount = await Post.find({ user: user.id }).count();

            const friendsCount = user.friends.length;

            return res.json({ user, postCount, friendsCount });
        } catch (err) {
            return next(err);
        }
    },

    async feed(req, res, next) {
        try {
            const user = await User.findById(req.userId);
            const { friends } = user;

            const posts = await Post
                .find({ user: { $in: [user.id, ...friends] } })
                .limit(50)
                .sort('-createdAt');

            return res.json(posts);
        } catch (err) {
            return next(err);
        }
    },

    async friend(req, res, next) {
        try {
            const user = await User.findById(req.params.id);

            if (!user) {
                return res.status(400).json({ error: 'User does not exist!' });
            }

            const me = await User.findById(req.userId);

            const myFriend = me.friends.indexOf(user.id);
            const hisFriend = user.friends.indexOf(me.id);

            if (myFriend !== -1 && hisFriend !== -1) {
                /* code snippet to unfriend */
                me.friends.splice(myFriend, 1);
                await me.save();

                user.friends.splice(hisFriend, 1);
                await user.save();

                return res.json(me);
            }

            /* code snippet to be friend */
            me.friends.push(user.id);
            await me.save();

            user.friends.push(me.id);
            await user.save();

            return res.json(me);
        } catch (err) {
            return next(err);
        }
    },

    async update(req, res, next) {
        try {
            const id = req.userId;

            const {
                name,
                username,
                password,
                confirmPassword,
            } = req.body;

            if (password && password !== confirmPassword) {
                return res.json({ error: 'Password doesn\'t match!' });
            }

            let data = {};

            if (name) {
                data = Object.assign({}, { name }, data);
            }

            if (username) {
                data = Object.assign({}, { username }, data);
            }

            const user = await User.findByIdAndUpdate(id, data, { new: true });

            if (password) {
                user.password = password;
                await user.save();
            }

            return res.json(user);
        } catch (err) {
            return next(err);
        }
    },
};
