const mongoose = require('mongoose');

const User = mongoose.model('User');

module.exports = {
    async signin(req, res, next) {
        try {
            const { email, password } = req.body;

            const user = await User.findOne({ email });

            if (!user) {
                return res.status(400).json({ error: 'User not found!' });
            }

            const checkPassword = await user.compareHash(password);

            if (!checkPassword) {
                return res.status(400).json({ error: 'Invalid password!' });
            }

            return res.json({
                user,
                token: user.generateToken(),
            });
        } catch (err) {
            return next(err);
        }
    },

    async signup(req, res, next) {
        try {
            const { email, username } = req.body;

            const userExist = await User.findOne({ $or: [{ email }, { username }] });

            if (userExist) {
                return res.status(400).json({ error: 'User already exists!' });
            }

            const user = await User.create(req.body);

            return res.json({
                user,
                token: user.generateToken(),
            });
        } catch (err) {
            return next(err);
        }
    },
};
