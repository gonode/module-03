const express = require('express');
const requireDir = require('require-dir');

const routes = express.Router();
const controllers = requireDir('./controllers');

const authMiddleware = require('./middlewares/auth');

/*
* Auth
* */
routes.post('/signup', controllers.authController.signup); // create account
routes.post('/signin', controllers.authController.signin); // login

/*
* Auth routes
* */
routes.use(authMiddleware);

/*
* User
* */
routes.get('/users/me', controllers.userController.me); // get user data
routes.put('/friend/:id', controllers.userController.friend); // befriend / unfriend
routes.put('/users', controllers.userController.update); // update user data
routes.get('/feed', controllers.userController.feed); // get feed

/*
* Posts
* */
routes.post('/posts', controllers.postController.create); // create post
routes.put('/posts/:id', controllers.postController.update); // update post
routes.put('/likes/:id', controllers.postController.toggle); // liked post
routes.delete('/posts/:id', controllers.postController.destroy); // delete post

/*
* Comments
* */
routes.post('/comments/:id', controllers.commentController.create); // comment post
routes.delete('/comments/:id', controllers.commentController.destroy); // delete comment

module.exports = routes;
